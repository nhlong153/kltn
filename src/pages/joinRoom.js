import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  Input,
  TextField,
} from "@mui/material";
import { Box, Container } from "@mui/system";
import React from "react";

const JoinRoomPage = () => {
  const playerRef = React.useRef(null);

  const videoJsOptions = {
    autoplay: true,
    controls: true,
    responsive: true,
    fluid: true,
    sources: [
      {
        src: "//vjs.zencdn.net/v/oceans.mp4",
        type: "video/mp4",
      },
    ],
  };

  const handlePlayerReady = (player) => {
    playerRef.current = player;

    player.on("waiting", () => {
      videojs.log("player is waiting");
    });

    player.on("dispose", () => {
      videojs.log("player will dispose");
    });
  };

  return (
    <div
      style={{
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundImage: "url(img/background.jpg)",
        width: "100%",
        height: "100vh",
      }}
    >
      <div className="row h-100 align-items-center">
        <div className="col-1"></div>
        <div className="col-4">
          <Card>
            <CardHeader title="Nhập mã phòng để tham gia cuộc gọi" />
            <CardContent>
              <TextField
                fullWidth
                margin="dense"
                label="Mã phòng"
                placeholder="Vui lòng nhập mã phòng"
              ></TextField>
            </CardContent>
            <CardActions>
              <Button fullWidth >
                Tham gia
              </Button>
            </CardActions>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default JoinRoomPage;
